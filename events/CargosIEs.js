const { Permissions, 
        MessageEmbed, 
        MessageActionRow,
        MessageSelectMenu, 
        Interaction }=require('discord.js')
module.exports = {
    name: 'interactionCreate',
    async execute(client, interaction, message, args) {
        if (!interaction.isSelectMenu()) return;
       
	const row = new MessageActionRow()
                .addComponents(
                    new MessageSelectMenu()
                    .setCustomId('del')
                    .setPlaceholder('Opções da Sala:')
					.addOptions([
						{
							label: '🔴 | Uniritter',
							value: 'uniritter',
						},
						{
							label: '🟣 | São Judas',
							value: 'sao-judas',
						},
                        {
							label: '🔵 | Una',
							value: 'una',
						},
						{
							label: '🟡 | AGES',
							value: 'ages',
						},
                        {
							label: '🟡 | IBMR ',
							value: 'ibmr',
						},
                        {
			    		    label: '🔵 | UAM',
							value: 'Una',
						},
                        {
							label: '🟡 | USJT',
							value: 'usjt',
						},
                        {
							label: '🟡 | Unifacs',
							value: 'unifacs',
						},
                        {
							label: '🟣 | Unifg(PE)',
							value: 'unifgpe',
						},
						{
							label: '🟣 | Unifg(BA)',
							value: 'unifgba',
						},
						{
							label: '🟣 | Fadergs',
							value: 'fadergs',
						},
						{
							label: '🟣 | Unisul',
							value: 'Unisul',
						},
						{
							label: '🟣 | FPB',
							value: 'fpb',
						},
						{
							label: '🟣 | Unibh',
							value: 'unibh',
						},
						{
							label: '🟣 | Unisociesc',
							value: 'Unisociesc',
						},
						{
							label: '🟣 | Unicuritiba',
							value: 'Unicuritiba',
						},
						{
							label: '🟣 | Faseh',
							value: 'faseh',
						},
					])
                );

            
                
    
        

        let DejaUnChannel = interaction.guild.channels.cache.find(c => c.topic == interaction.user.id)
        
        
        if(interaction.customId === "del") {
            if (interaction.values[0] == "delete") {
                const channel = interaction.channel
                channel.delete();
            }
        }

        if (interaction.customId == "select") {
            if (DejaUnChannel) return interaction.reply({content: '<:4247off:912015084035907665> Você já esta com uma faculdade selecionada', ephemeral: true})
            if (interaction.values[0] == "uniritter") {
                
                
                await interaction.member.roles.add('956251618259644506')
                await interaction.member.roles.add('951947587622142043')
                
               
               
            } else if (interaction.values[0] == "sao-judas") {

                await interaction.member.roles.add('960594167979974726')
                await interaction.member.roles.add('951947587622142043')
                
                   
            } else if (interaction.values[0] == "bancodedados") {
                
            
            }
        }
    }
}