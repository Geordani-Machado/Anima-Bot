module.exports = {
    name: 'ready',
    once: true,

    async execute(client) {
        console.log(`Bot conectado a ${client.user.username}`)

        var compteurStatus = 1
        setInterval(async () => {
            status =  [`aulas com os melhores professores`]
            compteurStatus = (compteurStatus + 1) % (status.length);
            client.user.setPresence({
                activities: [{
                    name: `${status[compteurStatus]}`,
                    type: "WATCHING",
                  }],
                  status: "online"})
        }, 50000);
    }
}
