const { Permissions, 
        MessageEmbed, 
        MessageActionRow,
        MessageSelectMenu, 
        Interaction }=require('discord.js')
module.exports = {
    name: 'interactionCreate',
    async execute(client, interaction) {
        if (!interaction.isSelectMenu()) return;
        
	const row = new MessageActionRow()
                .addComponents(
                    new MessageSelectMenu()
                    .setCustomId('del')
                    .setPlaceholder('Opções da Sala:')
					.addOptions([
						{
							label: '🗑️ Fechar Suporte',
							description: 'encerrar mentoria',
							value: 'delete',
						},
                        {
							label: '🤔 Solicitar Ajuda de Outro Mentor(a)',
							description: 'encerrar mentoria',
							value: 'ajuda',
						},
                        {
							label: '😍 Avaliar Mentor(a)',
							description: 'deixe uma avaliação e um recadinho especial ao Mentor(a) que lhe ajudou',
							value: 'avaliar',
						}
					])
                );
                
        let catégorie = "954118409157754930"
        let roleStaff = interaction.guild.roles.cache.get('954090427177381949')

        let DejaUnChannel = interaction.guild.channels.cache.find(c => c.topic == interaction.user.id)
        
        
        if(interaction.customId === "del") {
            if (interaction.values[0] == "delete") {
                const channel = interaction.channel
                channel.delete();
            }
        }

        if (interaction.customId == "select") {
            if (DejaUnChannel) return interaction.reply({content: '<:4247off:912015084035907665> Você já tem uma solicitação de suporte em aberto', ephemeral: true})
            if (interaction.values[0] == "logicaealgoritimo") {
                interaction.guild.channels.create(`ticket de ${interaction.user.username} para Logica de programação e Algoritimos` , {
                    type: 'GUILD_TEXT',
                    topic: `${interaction.user.id}`,
                    parent: `${catégorie}`,
                    permissionOverwrites: [
                        {   
                            id: interaction.guild.id,
                            deny: [Permissions.FLAGS.VIEW_CHANNEL]
                        },
                        {
                            id: interaction.user.id,
                            allow: [Permissions.FLAGS.VIEW_CHANNEL]
                        },
                        {
                            id: roleStaff,
                            allow: [Permissions.FLAGS.VIEW_CHANNEL]
                        }
                    ]
                }).then((c)=>{
                    const partenariat = new MessageEmbed()
                    .setTitle('Suporte Aberto - Ulife Mentoria')
                    .setDescription('Em Breve um Mentor ira lhe atender\'Facilite a vida dos nossos Mentores, descreva detalhadamente o sue problema ou divida para melhor lhe ajudar')
                    .setFooter('Ulife - Mentoria')
                    c.send({embeds: [partenariat], content: `${roleStaff} | ${interaction.user}`, components: [row]})
                    interaction.reply({content: `<:2003on:912015084405014558> Mentoria finalizada, esperamos que seu problema e duvidas tenhas sido resolvidos. <#${c.id}>`, ephemeral: true})
                })
            } else if (interaction.values[0] == "estrudedadosepoo") {

                interaction.guild.channels.create(`ticket de ${interaction.user.username} Estrutura de Dados e Progração Orientada a Objetos`, {
                    type: 'GUILD_TEXT',
                    topic: `${interaction.user.id}`,
                    parent: `${catégorie}`,
                    permissionOverwrites: [
                        {   
                            id: interaction.guild.id,
                            deny: [Permissions.FLAGS.VIEW_CHANNEL]
                        },
                        {
                            id: interaction.user.id,
                            allow: [Permissions.FLAGS.VIEW_CHANNEL]
                        },
                        {
                            id: roleStaff,
                            allow: [Permissions.FLAGS.VIEW_CHANNEL]
                        }
                    ]
                }).then((c)=>{
                    const plainte = new MessageEmbed()
                    .setTitle('Suporte Aberto - Ulife Mentoria')
                    .setDescription('Em Breve um Mentor ira lhe atender\'Facilite a vida dos nossos Mentores, descreva detalhadamente o sue problema ou divida para melhor lhe ajudar')
                    .setFooter('Ulife - Mentoria')
                    c.send({embeds: [plainte], content: `${roleStaff} | ${interaction.user}`, components: [row]})
                    interaction.reply({content: `<:2003on:912015084405014558> Mentoria finalizada, esperamos que seu problema e duvidas tenhas sido resolvidos. <#${c.id}>`, ephemeral: true})
                })
            } else if (interaction.values[0] == "bancodedados") {
                interaction.guild.channels.create(`ticket de ${interaction.user.username} para Banco de Dados`, {
                    type: 'GUILD_TEXT',
                    topic: `${interaction.user.id}`,
                    parent: `${catégorie}`,
                    permissionOverwrites: [
                        {   
                            id: interaction.guild.id,
                            deny: [Permissions.FLAGS.VIEW_CHANNEL]
                        },
                        {
                            id: interaction.user.id,
                            allow: [Permissions.FLAGS.VIEW_CHANNEL]
                        },
                        {
                            id: roleStaff,
                            allow: [Permissions.FLAGS.VIEW_CHANNEL]
                        }
                    ]
                }).then((c)=>{
                    const embed = new MessageEmbed()
                    .setTitle('Suporte Aberto - Ulife Mentoria')
                    .setDescription('Em Breve um Mentor ira lhe atender\'Facilite a vida dos nossos Mentores, descreva detalhadamente o sue problema ou divida para melhor lhe ajudar')
                    .setFooter('DevFr Support')
                    c.send({embeds: [embed], content: `${roleStaff} | ${interaction.user}`, components: [row]})
                    interaction.reply({content: `<:2003on:912015084405014558> Mentoria finalizada, esperamos que seu problema e duvidas tenhas sido resolvidos. <#${c.id}>`, ephemeral: true})
                })
                
            
            }
        }
    }
}