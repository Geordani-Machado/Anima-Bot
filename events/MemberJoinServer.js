const Discord = require("discord.js"); 
const { prefix } = require('../config/config.json');
const { Alunos } = require('../config/cargos-ies.json');
const { MessageCollector } = require("discord.js-collector");
module.exports = {
  
        name: 'guildMemberAdd',
        async execute(member, admin, message) {

            const novoMembroEmbed = new Discord.MessageEmbed()
            .setColor([153, 0, 76])
            .setTitle('Bem vindo(a) ao Discord da Ânima educação')
            .setDescription(`Autenticação Necessária para poder entrar para comunidade do Discord`)
            .addFields({
              name: 'Selecione uma opção:',
              value:
                'Reaja com 📨 se você autoriza a Ânima Educação a lhe enviar mensagens e reter seus dados para lhe autenticar como aluno no servidor e nas turmas correspondente a sua graduação e cadeiras. \n \n  Os dados aqui informados como RA e qual a sua instituição sera usados para lhe colocar nos cargos e canais correspondente a suas turmas e instituição.  \n \n seus dados não serão compartilhados , serão usados para fins internos. \n\n a Ânima segue as regras conforme a LGDP - seus dados são para uso interno, e os mesmo são criptografados e salvos em base nacional. :flag_br: ',
            });

          
            const sentDM = await admin.send({ embeds: [novoMembroEmbed] });
            // make sure you don't collect the bot's reactions
            const filter = (reaction, user) =>
              ['📨'].includes(reaction.emoji.name) && user.id === admin.id;
      
            sentDM.react('📨');
      
            // add a single options object only
            const collected = await sentDM.awaitReactions({ filter, maxEmojis: 1 });

          
            if (collected.first().emoji.name === '📨') {
              admin.send(' Bem vindo(a) a Âmnima Educação : )');
              admin.send(' Se você e um professor(a) solicite a um moderador os seus cargos');

              const botMessage = await admin.send("Por gentileza me informe o seu RA, o mesmo pode ser encontrado no seu portal do aluno ou no seu Ulife");
              const botMessage1 = await admin.send("Qual curso você esta fazendo?");
                  var fluxo = 1;

              MessageCollector.question({
                botMessage,
                user: member,
                onMessage: async (botMessage, message) => { // Every message sent by user will trigger this function.

                  if(fluxo == 1){
                    fluxo = 2;
                    await admin.send(`Seja bem vindo(a) aluno(a): '${message.content}'`);
                    
                  }
                }
            }); 

            if(fluxo == 2){

              MessageCollector.question({
                botMessage1,
                user: member,
                onMessage: async (botMessage1, message) => { // Every message sent by user will trigger this function.

                
                    await admin.send(`Curso definido como`);
                  
                }
            }); 

            }
          } 
        } 
       }
      
      

        
  