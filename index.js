
const {Client, Collection, Intents, Interaction} = require('discord.js');
const {readdirSync } = require('fs')//import fetch from 'node-fetch';
const Modals = require("discord-modals");
const jsh = require("discordjsh");

const { Modal, TextInputComponent, showModal } = require('discord-modals') // Now we extract the showModal method

const client = new Client({
    intents: [ 
		Intents.FLAGS.GUILDS, 
		Intents.FLAGS.GUILD_MEMBERS, 
		Intents.FLAGS.GUILD_BANS, 
		Intents.FLAGS.GUILD_EMOJIS_AND_STICKERS, 
		Intents.FLAGS.GUILD_INTEGRATIONS, 
		Intents.FLAGS.GUILD_WEBHOOKS, 
		Intents.FLAGS.GUILD_INVITES, 
		Intents.FLAGS.GUILD_VOICE_STATES, 
		Intents.FLAGS.GUILD_PRESENCES, 
		Intents.FLAGS.GUILD_MESSAGES, 
		Intents.FLAGS.GUILD_MESSAGE_REACTIONS, 
		Intents.FLAGS.GUILD_MESSAGE_TYPING, 
		Intents.FLAGS.DIRECT_MESSAGES, 
		Intents.FLAGS.DIRECT_MESSAGE_REACTIONS, 
		Intents.FLAGS.DIRECT_MESSAGE_TYPING,
		
	],
    restTimeOffset: 0,
    partials: ["USER", "CHANNEL", "GUILD_MEMBER", "MESSAGE", "REACTION"]
});


const { token, categoryID,voiceID,permissions} = require('./config/config.json');

Modals(client);
client.login(token);
client.commands = new Collection();



//|▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬| Commandes Handler |▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬|

const commandFiles = readdirSync('./command').filter(file => file.endsWith('.js'));
for (const file of commandFiles) {
	const command = require(`./command/${file}`);
	client.commands.set(command.name, command);
	}

//|▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬| Event Handler |▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬|

const eventFiles = readdirSync('./events').filter(file => file.endsWith('.js'));
for (const file of eventFiles) {
	const event = require(`./events/${file}`);
	if (event.once) {
		client.once(event.name, (...args) => event.execute(client, ...args));
	} else {
		client.on(event.name, (...args) => event.execute( client, ...args));
	}
}

/*
client.on('voiceStateUpdate', (Old, New) => {

    if(New.channelId == voiceID) {
		const user = client.user.fetch(New.id);

		//const member = New.guild.member(user);

        const channel = New.guild.channels.create("sala - Monitoria", { type: "GUILD_VOICE", parent: categoryID })
            .then((set) => {
                //set.overwritePermissions(New.user, permissions[0]);
                //set.overwritePermissions(New.guild.id, permissions[1]);
            
                //return New.VoiceChannel(New.guild.channels.get(id));
            });
			
    }

    if(Old.voiceChannelID) {
        let filter = (ch) =>
            (ch.parentID == categoryID)
            && (ch.id !== voiceID)
            && (Old.voiceChannelID == ch.id)
            && (Old.voiceChannel.members.size == 0);
        
        return Old.guild.channels
            .filter(filter)
            .forEach((ch) => ch.delete());
    }
});



client.on('messageCreate', (message) => {
	if(message.content === 'modal') {
		message.channel.send({
			content: 'Click below to apply for mod!',
			components:[{ type: 1, components: [{
				type: 2, custom_id: 'button', label: 'Apply', style: 'PRIMARY',
			}] }],
		});
	}
});

client.on('interactionCreate', async (interaction) => {
	await axios({
		method: 'POST',
		url: `https://discord.com/api/interactions/${interaction.id}/${interaction.token}/callback`,
		headers: {
			Authorization: `Bot ${client.token}`,
		},
		data: {
			type: 9,
			data: {
				title: 'Qual seu RA',
				custom_id: 'modal',
				components: [
					{
						type: 1,
						components: [
							{
								type: 4,
								custom_id: 'modalll',
								label: 'por gentileza informe o seu ra:',
								style: 1,
								min_length: 2,
								max_length: 400,
								required : true,
							},
						],
					},
				],
			},
		},
	});
});
*/

