const {MessageActionRow, MessageSelectMenu} = require('discord.js')
module.exports = {
    name: 'cargosies',
    usage: 'template',
    category: "mod",
    description: `Commande template.`,
    async execute(client, message, args) {
		message.delete()
        const row = new MessageActionRow()
			.addComponents(
				new MessageSelectMenu()
					.setCustomId('select')
					.setPlaceholder('Selecione a sua Instituição de ensino superior (IES)')
					.addOptions([
						{
							label: '🔴 | Uniritter',
							value: 'uniritter',
						},
						{
							label: '🟣 | São Judas',
							value: 'sao-judas',
						},
                        {
							label: '🔵 | Una',
							value: 'una',
						},
						{
							label: '🟡 | AGES',
							value: 'ages',
						},
                        {
							label: '🟡 | IBMR ',
							value: 'ibmr',
						},
                        {
			    		    label: '🔵 | UAM',
							value: 'Una',
						},
                        {
							label: '🟡 | USJT',
							value: 'usjt',
						},
                        {
							label: '🟡 | Unifacs',
							value: 'milton',
						},
                        {
							label: '🟣 | Unifg(PE)',
							value: 'unifgpe',
						},
						{
							label: '🟣 | Unifg(BA)',
							value: 'unifgba',
						},
						{
							label: '🟣 | Fadergs',
							value: 'fadergs',
						},
						{
							label: '🟣 | Unisul',
							value: 'Unisul',
						},
						{
							label: '🟣 | FPB',
							value: 'fpb',
						},
						{
							label: '🟣 | Unibh',
							value: 'unibh',
						},
						{
							label: '🟣 | Unisociesc',
							value: 'Unisociesc',
						},
						{
							label: '🟣 | Unicuritiba',
							value: 'Unicuritiba',
						},
						{
							label: '🟣 | Faseh',
							value: 'faseh',
						},
                       
					]),
			);

        message.channel.send({
            embeds: [{
                title: 'Selecione a sua faculdade (IES)',
                description: '**__ Sistema de Autenticação :__**\nSelecione abaixo no menu qual a sua faculdade :',
                color: "BLURPLE",
                footer: {text: 'Ânima Educação'}
            }],
            components: [row]
        })
    }
}
