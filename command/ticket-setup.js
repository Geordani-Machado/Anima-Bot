const {MessageActionRow, MessageSelectMenu} = require('discord.js')
module.exports = {
    name: 'ajuda',
    usage: 'template',
    category: "mod",
    description: `Commande template.`,
    async execute(client, message, args) {
		message.delete()
        const row = new MessageActionRow()
			.addComponents(
				new MessageSelectMenu()
					.setCustomId('select')
					.setPlaceholder('Select the type of ticket to create.')
					.addOptions([
						{
							label: '🤝 | Logica e algoritimos',
							description: 'Abrir suporte para Logica de progrmção e Algoritimos.',
							value: 'logicaealgoritimo',
						},
						{
							label: '📛 | Estrutura de dados e Programação orientada a objeto',
							description: 'Open a complaint ticket ',
							value: 'estrudedadosepoo',
						},
                        {
							label: '👥 | Banco de dados',
							description: 'Open a ticket to apply for recruitment',
							value: 'bandodedados',
						},
						{
							label: '👥 | Me Ajude com o meu exercicio',
							description: 'Open a ticket to apply for recruitment',
							value: 'execasa',
						},
					]),
			);

        message.channel.send({
            embeds: [{
                title: 'Solicitar Mentoria',
                description: '**__ Ulife - Mentoria :__**\nSolicitar ajuda a um Mentor para lhe ajudar e tirar suas duvidas',
                color: "BLURPLE",
                footer: {text: 'Ulife - mentoria'}
            }],
            components: [row]
        })
    }
}
